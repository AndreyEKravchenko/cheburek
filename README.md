# cheburek



## test task

Создать сайт по продаже чебуреков. Сайт должен уметь работать с бд. На странице должно быть реализовано отображение/редактирование/удаление чебурека через ajax. В визуальном плане - причесать, чтобы не голая таблица была

## SQL


CREATE TABLE `product` (
  `ID` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(300) NOT NULL,
  `picture` varchar(300) NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `product`
--

INSERT INTO `product` (`ID`, `name`, `description`, `picture`, `price`) VALUES
(0, 'Cheburek', 'классный чебурек с курицей', 'img/0.jpeg', 200),
(1, 'Cheburek 2', 'классный чебурек с курицей', 'img/1.jpeg', 300),
(2, 'Cheburek 3', 'классный чебурек с курицей', 'img/2.jpeg', 400),
(3, 'Ñ‡ÐµÐ±ÑƒÑ€ÐµÐº', 'классный чебурек с курицей', 'img/3.jpeg', 222);