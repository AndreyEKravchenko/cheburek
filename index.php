<?php
	$servername = "mysql-187279.srv.hoster.ru";
	$database = "srv187279_wp";
	$username = "srv187279_wp";
	$password = "password";
	
	$conn = mysqli_connect($servername, $username, $password, $database);
	if (!$conn) {
    	die("Connection failed: " . mysqli_connect_error());
	}
	
	
	global $conn;
	$sql = "SELECT * FROM `product`";
	$result = $conn->query($sql);

?>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Чебуреки здесь</title>
    <link rel="stylesheet" type="text/css" href="style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    
</head>

<body>
    <div id="content">
        <h1>Самые вкусные чебуреки - выбери свой</h1>        
      
        <br>
        <div class="grid">
<?php
			if ($result->num_rows > 0) {
				$i = 0;
	 			while($row = $result->fetch_assoc()) {
?>

	        	<div id = "<?php echo $i; ?>" class="item">
	                <div class="div">
	                    <img class="image" src="<?php echo $row["picture"]; ?>">
	                    <span class="name" data-target="name"><?php echo "название " . $row["name"]; ?></span>
	                    <span class="price" data-target="price"><?php echo "Цена:" . $row["price"]; ?></span>	                    
		            </div>
		            <button onclick="delproduct(<?php echo $row['ID']; ?>)">delete</button>
		            <button onclick="editproduct(<?php echo $row['ID']; ?>)">edit</button>
	            </div>
<?php
					$i++;
				}
			}
			mysqli_close($conn);
?>                      
        </div>


        <div id="myModal" class= "modal fade" role="dialog">
        	<div class="modal-dialog">
        		<div class="modal-content">
        			<div class="modal-header">
        				<button type="button" class="close" data-dismics="modal">&times;</button>
        				<h4 class="modal-title">Редактирование чебурека</h4>
        			</div>
        			<div class="modal-body">
        				<div class="form-group">
        					<label>Наименование</label>
        					<input type="text" id="name_id" class="form-control">
        				</div>
        				<div class="form-group">
        					<label>Цена</label>
        					<input type="text" id="price_id" class="form-control">
        				</div>
        				<input type="hidden" id = "prodId" class="form-control">
        			</div>
        			<div class="modal-footer">
        				<a href="#" id="save" class btn btn-primary pull-right">Записать</a>
        				<button type="button" class="btn ntn-default pull-left" data-dismiss="modal">Закрыть</button>
        			</div>
        		</div>
        	</div>
        </div>

    </body>
</html>

<script  type="text/javascript">
	function delproduct(id) {
		$(document). ready(function(){					
			$.ajax({
				type: "POST",
				url: "function.php",
				data: {prodid: id,
					action: "delete"
				},
				success: function(res) {
					console.log("Answer: " , res);
					if (res == 1) {
						console.log("res == 1");
						alert ("cheburek was deleted!");
						document.getElementById(id).style.display = "none";
					} 
					else if(res == 0) {
						alert ("can't delete!");
						console.log("res = 0");
					}
				}			
			});
		
		});
	}

	function editproduct(id) {		
		el = document.getElementById(id);
		var elname = el.getElementsByClassName("name")[0];
		var elprice = el.getElementsByClassName("price")[0];
		console.log(elname.innerText);
		console.log(elprice.innerText);
		
		 document.getElementById('name_id').value=elname.innerText;
		 document.getElementById('price_id').value=elprice.innerText;
		 document.getElementById('prodId').value=id;
		
		$('#myModal').modal('toggle');	
	}

	$('#save').click(function() {
		alert("запишем");
		var id = $('#prodId').val();
		var name = $('#name_id').val();
		var price = $('#price_id').val();

		console.log("id",id);

		$.ajax({
			method: "POST",
			url: "function.php",
			data: {prodid: id,
				productname: name,
				productprice: price,
				action: "update"
			},
			success: function(res){
				console.log(res);
				el = document.getElementById(id);
				el.getElementsByClassName("name")[0].innerText=name;
				el.getElementsByClassName("price")[0].innerText=price;
				$('#myModal').modal('toggle');
			}
		});
	});
</script>